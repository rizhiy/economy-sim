from pathlib import Path

from yacs.config import CfgNode as CN

_cfg = CN()

_cfg.POP_SIZE = int(1e4)
_cfg.STARTING_MONEY = 100
_cfg.NUM_ITERS = int(1e3)
_cfg.SKILL_DIST = 'UNIFORM'

SKILL_DIST_TYPES = {'UNIFORM', }


def get_default_cfg() -> CN:
    return _cfg.clone()


def load_cfg(cfg_path: Path) -> CN:
    cfg = get_default_cfg()
    cfg.merge_from_file(cfg_path)
    assert_cfg(cfg)
    return cfg


def assert_cfg(cfg: CN):
    assert cfg.SKILL_DIST in SKILL_DIST_TYPES, f"cfg.SKILL_DIST should be one of {SKILL_DIST_TYPES}"
