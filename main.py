import argparse
from pathlib import Path

import matplotlib.pyplot as plt
import numpy as np
from tqdm import tqdm

from config import load_cfg, get_default_cfg

parser = argparse.ArgumentParser()
parser.add_argument('--cfg', type=Path)
args = parser.parse_args()

if args.cfg is not None:
    cfg = load_cfg(args.cfg)
else:
    cfg = get_default_cfg()

if cfg.SKILL_DIST == 'UNIFORM':
    pop_skills = np.random.uniform(0, 1, size=cfg.POP_SIZE)
else:
    raise ValueError(f"{cfg.SKILL_DIST} is not a skill_dist_type")

pop_money = np.full(cfg.POP_SIZE, cfg.STARTING_MONEY, dtype=int)

alive_idxs = np.arange(0, cfg.POP_SIZE)
for idx in tqdm(range(cfg.NUM_ITERS)):
    alive_idxs = np.setdiff1d(alive_idxs, np.squeeze(np.argwhere(pop_money == 0), 1))

    np.random.shuffle(alive_idxs)
    left_idxs = alive_idxs[::2]
    right_idxs = alive_idxs[1::2]

    working_len = min(len(left_idxs), len(right_idxs))
    left_idxs = left_idxs[:working_len]
    right_idxs = right_idxs[:working_len]

    left_skills = pop_skills[left_idxs]
    right_skills = pop_skills[right_idxs]

    chance = np.random.uniform(0, 1, size=working_len)

    left_win = chance > right_skills / (left_skills + right_skills)
    win_idxs = np.concatenate([left_idxs[left_win], right_idxs[~left_win]])
    lose_idxs = np.concatenate([left_idxs[~left_win], right_idxs[left_win]])

    win_mask = np.zeros(cfg.POP_SIZE, np.bool)
    win_mask[win_idxs] = 1
    lose_mask = np.zeros(cfg.POP_SIZE, np.bool)
    lose_mask[lose_idxs] = 1

    pop_money[win_mask] += 1
    pop_money[lose_mask] -= 1

# TODO: Produced distribution looks wrong, find the bug
plt.hist(pop_money)
plt.show()
